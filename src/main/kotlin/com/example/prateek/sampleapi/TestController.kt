package com.example.prateek.sampleapi

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/hello")
class TestController {

    @GetMapping("/v1")
    fun getHelloMessage() : ResponseEntity<String> = ResponseEntity.ok("Hello world from v1!")

    @GetMapping("/v2")
    fun getMessage() : ResponseEntity<String> = ResponseEntity.ok("Hello world! from v2")
}